#include <iostream>
#include "list.h"

using namespace std;

List::~List() {
	for(Node *p; !isEmpty(); ){
		p=head->next;
		delete head;
		head=p;
	}
}

void List::printList() {//display element in link list.
	if (head == 0) {
		cout << "Empty list" << endl;
	}
	else {
		int count = 0;
		Node* show = head;
		while (show != 0) {
			count++;
			cout << "Node " << count << " ";
			cout << "value : " << show->info << endl;
			show = show->next;
		}
	}
}

void List::headPush(int a) {//add node to head.
	Node* add = new Node(a);
	if (head == 0) {
		head = add;
		tail = add;
	}
	else {
		add->next = head;
		head = add;
	}
}

void List::tailPush(int a) {//add node to tail.
	Node* add = new Node(a);
	if (head == 0) {
		head = add;
		tail = add;
	}
	else {
		tail->next = add;
		tail = add;
	}
}

int List::headPop() {//remove and return value from head.
	int value = head->info;
	head = head->next;
	return value;
}

int List::tailPop(){
	Node* newTail = head;//return value.
	while (newTail != 0) {
		newTail = newTail->next;
		if (newTail->next == 0) {
			break;
		}
	}
	int Value = newTail->info;//end return value.
	
	newTail = head;//remove value.
	while (newTail->next != tail) {

		newTail = newTail->next;
	}

	newTail->next = tail->next;
	tail = newTail;//end remove value.

	return Value;
}

void List::deleteNode(int num) {
	Node* preNum = head;//previous of current node.
	Node* curNum = head->next;//current node.
	
	while (curNum != 0) {
		if (curNum->info == num) {
			break;
		}
		else {
			preNum = curNum;
			curNum = curNum->next;
		}
	}
	cout << "Delete : " << curNum << endl;
	preNum->next = curNum->next;
}

bool List::isInList(int num) {
	Node* check = head;
	bool Check = false;
	while (check != 0) {
		if (check->info == num) {
			Check = true;
			break;
		}
		else {
			check = check->next;
		}
	}
	return Check;
}

