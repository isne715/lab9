#include <iostream>
#include "list.h"
using namespace std;

void check(List a, int num) {
	if (a.isInList(num) == 1) {
		cout << num << " is in list.";
	}
	else {
		cout << num << "is not in list.";
	}
}

void main()
{
	
	List a;
	
	a.tailPush(7);
	a.tailPush(9);
	a.headPush(3);
	a.headPush(4);
	a.tailPush(8);
	a.tailPush(6);
	a.headPush(2);
	a.headPush(1);
	a.headPush(3);
	cout << a.headPop() << endl;
	cout << a.tailPop() << endl;

	a.deleteNode(3);

	a.printList();

	int num = 4;//check isInList
	if (a.isInList(num) == 1) {
		cout << num << " is in list.";
	}
	else {
		cout << num << "is not in list.";
	}
	

	
	
}
